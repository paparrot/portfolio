import {ParsedContent} from "@nuxt/content/dist/runtime/types";

export default interface Project extends ParsedContent {
    title: string,
    slug: string,
    image: string,
    summary: string|null,
    tags: Array<string>|null,
    created_at: string,
}

