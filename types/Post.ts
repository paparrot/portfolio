import type {ParsedContent} from "@nuxt/content/dist/runtime/types";

interface Post extends ParsedContent {
    title: string,
    slug: string,
    image: string|null,
    summary: string|null,
    tags: Array<string>|null,
    created_at: string
}

export default Post;