---
title: Second post
slug: two
image: /media/minsk.jpeg
summary: post about dogs, cats, music, something cute
created_at: 23-01-2022
---


## So this is yet one post

As 2022 comes to a close, we at WordPress.com are preparing to ring in the new year. As we get ready to say goodbye to 2022, we’re thinking about how quickly time has flown by this past year. 

Beijing hosted the Winter Olympics in February, the James Webb Space Telescope started transmitting jaw-droppingly beautiful photos back to us on Earth, and the incredibly powerful Site Editor launched for WordPress sites!

With 2023 just around the corner, so too comes Bloganuary! Bloganuary is a writing challenge for everyone, whether you’re new to blogging, rediscovering your love of blogging, or blogging is your livelihood — we want all of you to join us.

Bloganuary is a month-long blogging challenge in January, where you’ll receive a daily writing prompt to inspire you to publish a post on your blog. You can respond to the prompts in any way you like: a story, a picture, a poem, a drawing, a recipe, or even a song. Anything goes!

By being a part of Bloganuary, you’ll join a global network of bloggers creating and/or strengthening solid writing habits. You’ll get to know fellow participants on the Bloganuary community site, where you can share tips, learn from each other, find a new audience for your blog, and make new blogging friends.

To show others what you’ll be up to in January and encourage them to join in the Bloganuary fun, here’s a badge you can add to your blog (right-click and save/download to your computer):


Join the Bloganuary challenge, stay motivated, and start the new year off on the write track!