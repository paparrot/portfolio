// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
        '@nuxtjs/tailwindcss', 
        '@nuxtjs/color-mode', 
        '@nuxt/content',
        'nuxt-icon'
    ],
    css: [
        '@/assets/css/main.css'
    ],
    colorMode: {
        classSuffix: ""
    },
    app: {
        head: {
            charset: "utf-8",
            htmlAttrs: {
                lang: "en",
            },
            title: "Paparrot",
            meta: [
                {
                    name: "description",
                    content: "Web developer"
                }
            ]
        }
    }
})