# Full customisable Nuxt portfolio template

## Features
- Dark mode
- Blog
- Portfolio projects

## Install

1. Clone project
```bash
git clone ...
```

2. Install dependencies
```bash
# npm
npm install

#yarn
yarn install
```

3. Run for development
```bash
yarn dev
```

## Customize

You can customize all parts of the project how you want.

## Roadmap
- [x] Dark mode
- [x] Blog
- [x] Projects
- [] Responsive layout
- [] Multilanguage
- [] Navigation customizations